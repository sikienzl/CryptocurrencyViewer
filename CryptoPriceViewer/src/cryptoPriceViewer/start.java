package cryptoPriceViewer;

import java.io.IOException;

import cryptoPrice.controller.CryptocurrencyController;

public class start {
	public static void main(String [] args) throws IOException
	{
		
		CryptocurrencyController cryptocurrencyController = new CryptocurrencyController();
		cryptocurrencyController.initMethods();
		cryptocurrencyController.showView();
	}
}
