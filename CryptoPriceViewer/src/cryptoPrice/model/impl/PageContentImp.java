package cryptoPrice.model.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

import cryptoPrice.model.PageContent;

public class PageContentImp implements PageContent {

	/*TODO: use factorymethod*/
	private StringBuilder sbContent;
	public PageContentImp(String destinationUlr) throws IOException
	{
		URL url = new URL(destinationUlr);
		BufferedReader br = null;

        try {

            br = new BufferedReader(new InputStreamReader(url.openStream()));

            String line;

            sbContent = new StringBuilder();

            while ((line = br.readLine()) != null ) 
            {
        		sbContent.append(line);
            }
            setContent(sbContent);
        } finally {

            if (br != null) {
                br.close();
            }
        }
	}
	@Override
	public void setContent(StringBuilder sbContent) 
	{
		this.sbContent = sbContent;
	}
	@Override
	public StringBuilder getContent()
	{
		return sbContent;
	}
	
}
