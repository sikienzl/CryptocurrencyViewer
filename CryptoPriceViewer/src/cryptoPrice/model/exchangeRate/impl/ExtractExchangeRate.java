package cryptoPrice.model.exchangeRate.impl;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.w3c.dom.Node;
import org.w3c.dom.Element;


public class ExtractExchangeRate {
	private List<ExchangeRateImp> exchangeRateImpList;
	private String destinationUrl;
	
	public ExtractExchangeRate(String destinationUrl)
	{
		this.destinationUrl = destinationUrl;
		exchangeRateImpList = new LinkedList<ExchangeRateImp>();
	}
	
	public List<ExchangeRateImp> extractFromContentList()
	{
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = null;
		
		try {
			dBuilder = dbFactory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Document doc = null;
		try {
			doc = dBuilder.parse(destinationUrl);
		} catch (SAXException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		doc.getDocumentElement().normalize();

		
		NodeList currencyList = doc.getElementsByTagName("Cube");


		for (int element = 0; element < currencyList.getLength(); element++) {

			Node nodeCurrency = currencyList.item(element);
			if (nodeCurrency.getNodeType() == Node.ELEMENT_NODE) 
			{

				Element currency = (Element) nodeCurrency;
				if(!currency.getAttribute("currency").isEmpty()
					)
				{
					ExchangeRateImp exchangeRateImp = new ExchangeRateImp();
					exchangeRateImp.setCurrency(currency.getAttribute("currency"));
					exchangeRateImp.setRate(Double.parseDouble(currency.getAttribute("rate")));
						exchangeRateImpList.add(exchangeRateImp);
				}
				
			}
		}
		return exchangeRateImpList;
	}


}
