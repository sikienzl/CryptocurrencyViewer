package cryptoPrice.model.exchangeRate.impl;

import cryptoPrice.model.exchangeRate.ExchangeRate;

public class ExchangeRateImp implements ExchangeRate {
	private String currency;
	private double rate;
	
	@Override
	public void setCurrency(String currency)
	{
		this.currency = currency;
	}
	@Override
	public String getCurrency()
	{
		return currency;
	}
	@Override
	public void setRate(double rate)
	{
		this.rate = rate;
	}
	@Override
	public double getRate()
	{
		return rate;
	}
	
	

}
