package cryptoPrice.model.exchangeRate;

public interface ExchangeRate {
	void setCurrency(String currency);
	String getCurrency();
	void setRate(double rate);
	double getRate();

}
