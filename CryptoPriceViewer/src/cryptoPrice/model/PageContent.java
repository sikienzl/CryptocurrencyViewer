package cryptoPrice.model;

public interface PageContent
{
	
	void setContent(StringBuilder sbContent);
	StringBuilder getContent();
}
