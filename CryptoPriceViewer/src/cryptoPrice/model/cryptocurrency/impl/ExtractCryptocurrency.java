package cryptoPrice.model.cryptocurrency.impl;

import java.util.LinkedList;
import java.util.List;

public class ExtractCryptocurrency {
	private StringBuilder webContent;
	private List<Cryptocurrency> cryptocurrencyList;
	
	public ExtractCryptocurrency(StringBuilder webContent)
	{
		this.webContent = webContent;
		cryptocurrencyList = new LinkedList<Cryptocurrency>();
		extractCryptocurrencyFromContentList();
	}

	
		
	
	public List<Cryptocurrency> extractCryptocurrencyFromContentList()
	{
		String [] line = webContent.toString().replace("[    ", "").replace("]", "").replaceAll(" ", "").replace(",{", "").replace("{", "").split("}");
		
		
		for(int i = 0; i < line.length; i++)
		{
			Cryptocurrency cryptocurrency = new Cryptocurrency();
			String [] line2 = line[i].split(",");
			
			cryptocurrency.setId(line2[0].replace("\"", "").split(":")[1]);
			cryptocurrency.setName(line2[1].replace("\"", "").split(":")[1]);
			cryptocurrency.setSymbol(line2[2].replace("\"", "").split(":")[1]);
			cryptocurrency.setRank(Integer.parseInt(line2[3].replace("\"", "").split(":")[1]));
			cryptocurrency.setPrice_usd(doubleNullCheck(line2[4].replace("\"", "").split(":")[1]));
			cryptocurrency.setPrice_btc(doubleNullCheck(line2[5].replace("\"", "").split(":")[1]));
			cryptocurrency.setTwentyFourHours_volume_usd(doubleNullCheck(line2[6].replace("\"", "").split(":")[1]));
			cryptocurrency.setMarket_cap_usd(doubleNullCheck(line2[7].replace("\"", "").split(":")[1]));
			cryptocurrency.setAvailable_supply(doubleNullCheck(line2[8].replace("\"", "").split(":")[1]));
			cryptocurrency.setTotal_supply(doubleNullCheck(line2[9].replace("\"", "").split(":")[1]));
			cryptocurrency.setMax_supply(doubleNullCheck(line2[10].replace("\"", "").split(":")[1]));
			cryptocurrency.setPercent_change_1h(doubleNullCheck(line2[11].replace("\"", "").split(":")[1]));
			cryptocurrency.setPercent_change_24h(doubleNullCheck(line2[12].replace("\"", "").split(":")[1]));
			cryptocurrency.setPercent_change_7d(doubleNullCheck(line2[13].replace("\"", "").split(":")[1]));
			cryptocurrency.setLast_updated(Integer.parseInt(line2[14].replace("\"", "").split(":")[1]));
			
			cryptocurrencyList.add(cryptocurrency);
		}
		
		return cryptocurrencyList;
	}
	private double doubleNullCheck(String value)
	{
		double doubleValue = 0;
		
		if(!value.equals("null"))
		{
			doubleValue = Double.parseDouble(value);
		}
		
		return doubleValue;
	}
}
