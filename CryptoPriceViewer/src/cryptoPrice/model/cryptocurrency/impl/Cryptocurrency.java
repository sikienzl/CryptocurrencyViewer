package cryptoPrice.model.cryptocurrency.impl;

public class Cryptocurrency {
	private String id;
	private String name;
	private String symbol;
	private int rank;
	private double price_usd;
	private double price_btc;
	private double twentyFourHours_volume_usd;
	private double market_cap_usd;
	private double available_supply;
	private double total_supply;
	private double max_supply;
	private double percent_change_1h;
	private double percent_change_24h;
	private double percent_change_7d;
	private int last_updated;
	
	
	public void setId(String id)
	{
		this.id = id;
	}
	
	public String getId()
	{
		return id;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	public String getName()
	{
		return name;
	}
	
	public void setSymbol(String symbol)
	{
		this.symbol = symbol;
	}
	public String getSymbol()
	{
		return symbol;
	}
	public void setRank(int rank)
	{
		this.rank = rank;
	}
	public int getRank()
	{
		return rank;
	}
	public void setPrice_usd(double price_usd)
	{
		this.price_usd = price_usd;
	}
	public double getPrice_usd()
	{
		return price_usd;
	}
	
	public void setPrice_btc(double price_btc)
	{
		this.price_btc = price_btc;
	}
	
	public double getPrice_btc()
	{
		return price_btc;
	}
	
	public void setTwentyFourHours_volume_usd(double twentyFourHours_volume_usd)
	{
		this.twentyFourHours_volume_usd = twentyFourHours_volume_usd;
	}
	
	public double getTwentyFourHours_volume_usd()
	{
		return twentyFourHours_volume_usd;
	}
	
	public void setMarket_cap_usd(double market_cap_usd)
	{
		this.market_cap_usd = market_cap_usd;
	}
	public double getMarket_cap_usd()
	{
		return market_cap_usd;
	}
	
	public void setAvailable_supply(double available_supply)
	{
		this.available_supply = available_supply;
	}
	
	public double getAvailable_supply()
	{
		return available_supply;
	}
	
	public void setTotal_supply(double total_supply)
	{
		this.total_supply = total_supply;
	}
	
	public double getTotal_supply()
	{
		return total_supply;
	}
	
	public void setMax_supply(double max_supply)
	{
		this.max_supply = max_supply;
	}
	
	public double getMax_supply()
	{
		return max_supply;
	}
	public void setPercent_change_1h(double percent_change_1h)
	{
		this.percent_change_1h = percent_change_1h;
	}
	public double getPercent_change_1h()
	{
		return percent_change_1h;
	}
	
	public void setPercent_change_24h(double percent_change_24h)
	{
		this.percent_change_24h = percent_change_24h;
	}
	
	public double getPercent_change_24h()
	{
		return percent_change_24h;
	}
	
	public void setPercent_change_7d(double percent_change_7d)
	{
		this.percent_change_7d = percent_change_7d;
	}
	
	public double getPercent_change_7d()
	{
		return percent_change_7d;
	}
	
	public void setLast_updated(int last_updated)
	{
		this.last_updated = last_updated;
	}
	public int getLast_updated()
	{
		return last_updated;
	}
}
