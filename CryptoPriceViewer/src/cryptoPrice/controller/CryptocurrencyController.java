package cryptoPrice.controller;

import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.IOException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JFrame;

import cryptoPrice.model.cryptocurrency.impl.Cryptocurrency;
import cryptoPrice.model.cryptocurrency.impl.ExtractCryptocurrency;
import cryptoPrice.model.exchangeRate.ExchangeRate;
import cryptoPrice.model.exchangeRate.impl.ExchangeRateImp;
import cryptoPrice.model.exchangeRate.impl.ExtractExchangeRate;
import cryptoPrice.model.impl.PageContentImp;
import cryptoPrice.view.Cryptocurrencywindow;
import cryptoPrice.view.WindowCloseListener;

public class CryptocurrencyController {
	
	private String ticker;
	private PageContentImp pageContentImp;
	private ExtractCryptocurrency extractCryptoCurreny;
	private final String URLECB = "https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml";
	private final String URLCOINMARKET = "https://api.coinmarketcap.com/v1/ticker/";
	private ExchangeRateImp usdRate;
	private ExchangeRateImp jpyRate;
	private ExchangeRateImp bgnRate;
	private ExchangeRateImp czkRate;
	private ExchangeRateImp dkkRate;
	private ExchangeRateImp gbpRate;
	private ExchangeRateImp hufRate;
	private ExchangeRateImp plnRate;


	public CryptocurrencyController()
	{
		ticker = "";
		//createTicker();
		
		//getCurrentExchangeRate();

		createTicker();
	}

	public void initMethods() {
		extractAndSetRate();
	}
	private void createTicker() 
	{
		
		try {
			pageContentImp = new PageContentImp(URLCOINMARKET);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		extractCryptoCurreny = new ExtractCryptocurrency(pageContentImp.getContent());
		
		for(Cryptocurrency crypt : extractCryptoCurreny.extractCryptocurrencyFromContentList())
		{
			
			ticker += " Cryptocurrency: " +crypt.getName() + " price in BTC: " + crypt.getPrice_btc(); /*+ " price in USD: " + crypt.getPrice_usd() + " Change after 1h: " + crypt.getPercent_change_1h() + "% ";
			*/
		}
	}

	public double calculateEURPrice(double usd_price) {
		double eurPrice = 0;

		eurPrice = usd_price * calculateAndReturnEURCourse();

		return eurPrice;
	}

	private void extractAndSetRate() {
		ExtractExchangeRate extractExchangeRate = new ExtractExchangeRate(URLECB);
		List<ExchangeRateImp> exchangeRateImpList = extractExchangeRate.extractFromContentList();
		for(int exchangeRateCount = 0; exchangeRateCount < exchangeRateImpList.size(); exchangeRateCount++) {

			ExchangeRate exchangeRate = exchangeRateImpList.get(exchangeRateCount);
			switch (exchangeRate.getCurrency())
			{
				case "USD":
					usdRate = new ExchangeRateImp();
					usdRate.setCurrency(exchangeRate.getCurrency());
					usdRate.setRate(exchangeRate.getRate());
				case "JYP":
					jpyRate = new ExchangeRateImp();
					jpyRate.setCurrency(exchangeRate.getCurrency());
					jpyRate.setRate(exchangeRate.getRate());

			}
		}
	}

	private double calculateAndReturnEURCourse() {
		double eurCourse = 1 / usdRate.getRate();
		return eurCourse;
	}
	
	public void showView()
	{
		JFrame frame= new JFrame("Cryptocurrency viewer");
    	//Cryptocurrencywindow cryptocurrencywindow = new Cryptocurrencywindow(ticker);
		Cryptocurrencywindow cryptocurrencywindow =
				new Cryptocurrencywindow(extractCryptoCurreny.extractCryptocurrencyFromContentList(), this);
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		frame.setBounds(30, 30, 1250, 470);
		frame.setVisible(true);
		frame.addWindowListener(new WindowCloseListener());
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new CardLayout());
		frame.getContentPane().add(cryptocurrencywindow);
		frame.setResizable(true);

		
		
				  
	}


	

	

}
