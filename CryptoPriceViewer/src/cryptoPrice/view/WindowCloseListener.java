package cryptoPrice.view;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class WindowCloseListener extends WindowAdapter {
	public void windowClosing(WindowEvent e)
	{
		e.getWindow().dispose();
		System.exit(0);
	}

}
