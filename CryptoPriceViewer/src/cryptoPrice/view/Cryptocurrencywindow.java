package cryptoPrice.view;

import cryptoPrice.controller.CryptocurrencyController;
import cryptoPrice.model.cryptocurrency.impl.Cryptocurrency;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import java.awt.*;
import java.util.List;

public class Cryptocurrencywindow extends JPanel {
    private CryptocurrencyController cryptocurrencyController;
    public Cryptocurrencywindow(List<Cryptocurrency> cryptoCurrencyElements, CryptocurrencyController cryptocurrencyController) {
        String[] columnNames = {"Id", "Name", "Symbol", "Rank", "price USD", "price EUR",
                "price BTC", "twenty four hours volume", "market cap usd",
                "available supply", "total supply", "max supply", "percent change 1h",
                "percent change 24h", "percent change 7d", "last updated"};

        DefaultTableModel cryptoCurrencyTableModel = new DefaultTableModel(columnNames, 0);
        this.cryptocurrencyController = cryptocurrencyController;

        JTable table = new JTable(cryptoCurrencyTableModel);
        table.setPreferredScrollableViewportSize(new Dimension(1200, 400));
        DefaultTableModel cryptoCurrencyDataTableModel = (DefaultTableModel)table.getModel();


        this.add(new JScrollPane(table));
        Object cryptoData[] = new Object[16];

        for (int cryptoCount = 0; cryptoCount < cryptoCurrencyElements.size(); cryptoCount++) {
            cryptoData[0] = cryptoCurrencyElements.get(cryptoCount).getId();
            cryptoData[1] = cryptoCurrencyElements.get(cryptoCount).getName();
            cryptoData[2] = cryptoCurrencyElements.get(cryptoCount).getSymbol();
            cryptoData[3] = cryptoCurrencyElements.get(cryptoCount).getRank();
            cryptoData[4] = cryptoCurrencyElements.get(cryptoCount).getPrice_usd();
            cryptoData[5] = cryptocurrencyController.calculateEURPrice(cryptoCurrencyElements.get(cryptoCount).getPrice_usd());
            cryptoData[6] = cryptoCurrencyElements.get(cryptoCount).getPrice_btc();
            cryptoData[7] = cryptoCurrencyElements.get(cryptoCount).getTwentyFourHours_volume_usd();
            cryptoData[8] = cryptoCurrencyElements.get(cryptoCount).getMarket_cap_usd();
            cryptoData[9] = cryptoCurrencyElements.get(cryptoCount).getAvailable_supply();
            cryptoData[10] = cryptoCurrencyElements.get(cryptoCount).getTotal_supply();
            cryptoData[11] = cryptoCurrencyElements.get(cryptoCount).getMax_supply();
            cryptoData[12] = cryptoCurrencyElements.get(cryptoCount).getPercent_change_1h();
            cryptoData[13] = cryptoCurrencyElements.get(cryptoCount).getPercent_change_24h();
            cryptoData[14] = cryptoCurrencyElements.get(cryptoCount).getPercent_change_7d();
            cryptoData[15] = cryptoCurrencyElements.get(cryptoCount).getLast_updated();
            cryptoCurrencyDataTableModel.addRow(cryptoData);
        }


    }
}
